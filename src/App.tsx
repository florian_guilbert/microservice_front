import React from 'react';
import './App.css';
import Products from './admin/Products';
import ProductsCreate from './admin/ProductsCreate';
import ProductsUpdate from './admin/ProductsUpdate';
import Main from './main/Main';
import {BrowserRouter, Route} from 'react-router-dom';


function App() {
  return (
    <div className="App">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
      integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossOrigin="anonymous"/>


            <BrowserRouter>
              <Route path='/' exact component={Main}/>
              <Route path='/admin/products' exact component={Products}/>
              <Route path='/admin/products/create' exact component={ProductsCreate}/>
              <Route path='/admin/products/:id/update' exact component={ProductsUpdate}/>
            </BrowserRouter>


    </div>
  );
}

export default App;
