import React, {SyntheticEvent, useState, useEffect, PropsWithRef} from 'react';
import {Redirect} from 'react-router-dom'
import {Product} from '../interfaces/product'
import Wrapper from './Wrapper'

const ProductsUpdate = (props: PropsWithRef<any>) => {
  const [title, setTitle] = useState('');
  const [image, setImage] = useState('');
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    (
      async() => {
        const response = await fetch(`/backendadmin/api/products/${props.match.params.id}`); //id from App.tsx route
        const product: Product = await response.json()
        setTitle(product.title);
        setImage(product.image);
      }
    )();
    // eslint-disable-next-line
  }, []);

  const submit = async (e: SyntheticEvent) => {
    e.preventDefault();

    await fetch(`/backendadmin/api/products/${props.match.params.id}`, {
        method: 'PUT',
        headers: {'Content-type': 'application/json'},
        body: JSON.stringify({ //linked to form inputs name
          title: title,
          image //same as image: image
        })
    });

    setRedirect(true)
  }

  if(redirect){
    return <Redirect to={'/admin/products'}/>
  }

  return (
    <Wrapper>
      <form onSubmit={submit}>
        <div className="form-group">
          <label>Tile</label>
          <input type="text" className="form-control" name="title"
            defaultValue={title}
            onChange={e => setTitle(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label>Image</label>
          <input type="text" className="form-control" name="image"
            defaultValue={image}
            onChange={e => setImage(e.target.value)}
          />
        </div>
        <button className="btn btn-outline-secondary">Save</button>
      </form>
    </Wrapper>
  )
}

export default ProductsUpdate
