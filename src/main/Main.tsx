import React, {useEffect, useState} from 'react';
import {Product} from '../interfaces/product'
import NavSideBar from '../admin/components/NavSideBar';

const Main = () => {

    const [products, setProducts] = useState([] as Product[])
    useEffect( () => {
      const getProducts = async() => {
        const response = await fetch('/backendadmin/api/products/')
        const data = await response.json()
        console.log(data)
        setProducts(data)
      };
      getProducts()
    }, []);

    const like = async (id: number) => {
      const response = await fetch(`/backendmain/api/products/${id}/like`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'}
      });
      const data = await response.json()
      if(data['message'] == 'fail_already_likes'){
        alert("user " + data['user'] + " out of 5 (chosen randomly) already likes this product");
      }
      else{
        alert("user " + data['user'] + " out of 5 (chosen randomly) liked this product !");
        setProducts(products.map(
          (p: Product) => {
            if (p.id === id){
              p.likes++;
            }
            return p;
          }
        ));
      }
    }

  return (
    <main>
      <NavSideBar />
      <div className="album py-5 bg-light">
        <div className="container">
          <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

          {products.map( (p: Product) => {

            return (
              <div className="col" key={p.id}>
                <div className="card shadow-sm">
                  <img src={p.image} height="180"/>
                  <div className="card-body">
                    <p className="card-text">{p.title}</p>
                    <div className="d-flex justify-content-between align-items-center">
                      <div className="btn-group">
                        <button type="button" className="btn btn-sm btn-outline-secondary"
                          onClick={() => like(p.id)}>
                        Like
                        </button>
                      </div>
                      <small className="text-muted">{p.likes} likes</small>
                    </div>
                  </div>
                </div>
              </div>
            )}

          )}

          </div>
        </div>
      </div>

    </main>
  )
}

export default Main
