const proxy = require('http-proxy-middleware');
module.exports = app =>{
  app.use(
    '/backendmain',
    proxy({
      target:'http://backendmain:5000',
      changeOrigin: true,
      pathRewrite: {'^/backendmain':''}
    })
  );
  app.use(
    '/backendadmin',
    proxy({
      target:'http://backendadmin:8000',
      changeOrigin: true,
      pathRewrite: {'^/backendadmin':''}
    })
  );

};
